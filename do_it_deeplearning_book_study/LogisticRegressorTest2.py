import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import SGDClassifier


if __name__ == '__main__':
    cancer = load_breast_cancer()

    x = cancer.data
    y = cancer.target

    x_train, x_test, y_train, y_test = train_test_split(x, y, stratify=y, test_size=0.2, random_state=42)
    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, stratify=y_train, test_size=0.2, random_state=42)

    class LogisticNeuron:
        def __init__(self, learning_rate=0.1):
            self.w = None
            self.b = None
            self.losses = []
            self.val_losses = []
            self.w_history = []
            self.lr = learning_rate

        def forpass(self, x):
            z = np.sum(x * self.w) + self.b
            return z

        def backprop(self, x, err):
            w_grad = x * err
            b_grad = 1 * err

            return w_grad, b_grad

        def activation(self, z):
            z = np.clip(z, -100, None)
            a = 1 / (1 + np.exp(-z))
            return a

        def fit(self, x, y, x_val=None, y_val=None, epochs=100):

            self.w = np.ones(x.shape[1])
            self.b = 0
            self.w_history.append(self.w.copy())
            np.random.seed(42)

            for i in range(epochs):
                loss = 0
                indexes = np.random.permutation(np.arange(len(x)))
                for j in indexes:
                    z = self.forpass(x[j])
                    a = self.activation(z)
                    err = -(y[j] - a)
                    w_grad, b_grad = self.backprop(x[j], err)
                    self.w -= w_grad
                    self.b -= b_grad

                    self.w_history.append(self.w.copy())
                    a = np.clip(a, 1e-10, 1-1e-10)
                    
                    loss += -(y[j]*np.log(a) + (1 - y[j])*np.log(1-a))

                self.losses.append(loss/len(x))
                self.update_val_loss(x_val, y_val)

        def update_val_loss(self, x_val, y_val):
            if x_val is None:
                return
            val_loss = 0
            for i in range(len(x_val)):
                z = self.forpass(x_val[i])
                a = self.activation(z)
                a = np.clip(a, 1e-10, 1-1e-10)
                val_loss += -(y_val[i]*np.log(a) + (1 - y_val[i])*np.log(1-a))
            self.val_losses.append(val_loss / len(y_val))

        def predict(self, x):
            z = [self.forpass(x_i) for x_i in x]
            # a = self.activation(np.array(z))
            return np.array(z) > 0

        def print_losses(self):
            print(self.losses)

        def score(self, x, y):
            print(np.mean(self.predict(x) == y))


    train_mean = np.mean(x_train, axis=0)
    train_std  = np.std(x_train, axis=0)

    x_train_scaled = (x_train - train_mean) / train_std
    x_val_scaled = (x_val - train_mean) / train_std

    # # 일반 훈련데이터
    # neuron = LogisticNeuron()
    # neuron.fit(x_train, y_train)
    # neuron.score(x_val, y_val)
    #
    # w2 = []
    # w3 = []
    #
    # for w in neuron.w_history:
    #     w2.append(w[2])
    #     w3.append(w[3])
    #
    # plt.plot(w2,w3)
    # plt.plot(w2[-1], w3[-1], 'ro')
    # plt.show()
    #
    #
    # # 표준화, 전처리
    # neuron = LogisticNeuron()
    # neuron.fit(x_train_scaled, y_train)
    # w2 = []
    # w3 = []
    # for w in neuron.w_history:
    #     w2.append(w[2])
    #     w3.append(w[3])
    #
    # plt.plot(w2, w3)
    # plt.plot(w2[-1], w3[-1], 'ro')
    # plt.show()
    #
    # neuron.score(x_val_scaled, y_val)

    # 검증 손실 모델
    neuron = LogisticNeuron()
    neuron.fit(x_train_scaled, y_train, x_val=x_val_scaled, y_val=y_val)

    print(neuron.losses)
    print(neuron.val_losses)

    plt.ylim(0, 0.3)
    plt.plot(neuron.losses)
    plt.plot(neuron.val_losses)
    plt.legend(['train_loss', 'val_loss'])
    plt.show()

    neuron = LogisticNeuron()
    neuron.fit(x_train_scaled, y_train, epochs=20)
    neuron.score(x_val_scaled, y_val)


